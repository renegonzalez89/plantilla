<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Contenido de página de inicio -->
<!-- CARRUSEL PRODUCTOS -->
<div class="carousel">
    <div id="lista">
        <div class="imgFrontal">
            <img class="imagenSlider" src="<?php echo get_theme_file_uri('asset/src/inicio/sliderUno.png') ?>" alt="Item" />
            <div class="textoSlider">
                <div class="imgLogoBlanco"><img src="<?php echo get_theme_file_uri('asset/src/inicio/logoBlanco.png'); ?>" alt="Zapatito"></div>
                <div class="titulo">Lorem ipsum dolor sit.</div>
                <div class="texto">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore inventore nulla quidem aliquam ratione, itaque, earum eos labore, veniam quaerat mollitia minima nesciunt numquam repellat?</div>
                <button>Comprar ahora</button>
            </div>
        </div>
       
    </div>
    <button aria-label="Anterior" class="prev"><i class="fa-solid fa-chevron-left"></i></button>
    <button aria-label="Siguiente" class="next"><i class="fa-solid fa-chevron-right"></i></button>
    <div role="tablist" class="puntos"></div>
</div>
<div class="seccionCatalogo">
    <div class="catalogoUno cardCatalogo">
        <a href="categoria-producto/calzado-industrial">
            <img src="<?php echo get_theme_file_uri('asset/src/inicio/catalogoTres.png') ?>" alt="">
            <p>Calzado Industrial</p>
        </a>
    </div>
    <div class="catalogoDos cardCatalogo">
        <a href="categoria-producto/equipo-industrial">
            <img src="<?php echo get_theme_file_uri('asset/src/inicio/catalogoDos.png') ?>" alt="">
            <p>Equipo Industrial</p>
        </a>
    </div>
    <div class="catalogoTres cardCatalogo">
        <a href="categoria-producto/calzado-escolar">
            <img src="<?php echo get_theme_file_uri('asset/src/inicio/catalogoUno.png') ?>" alt="">
            <p>Calzado Escolar</p>
        </a>
    </div>
</div>
<div class="seccionArticulos">
    <div class="lateralIzquierda">

        <nav class="navMenuCategorias">
            <ul class="main-nav-categorias">
                <li class="tituloMenu">Catálogo</li>
                <?php wp_nav_menu(array('theme_location' => 'catalogo')); ?>
            </ul>
        </nav>

        <div class="seccion-ofertas">
           
            <?php echo do_shortcode("[content_ofertas]")?>
        </div>
    </div>
    <div class="lateralDerecha">
        <div class="seccion-sliders">
           <?php echo do_shortcode("[woo_product_slider id='87']")?>
       </div>
       <div class="seccion-categoria">
            <div class="categoriaUno cardCategoria">
                <a href="categoria-producto/calzado-industrial">
                    <img src="<?php echo get_theme_file_uri('asset/src/inicio/banner1.png') ?>" alt="">
                    <p>Ofertas</p>
                </a>
            </div>
            <div class="categoriaDos cardCategoria">
                <a href="categoria-producto/calzado-industrial">
                    <img src="<?php echo get_theme_file_uri('asset/src/inicio/banner2.png') ?>" alt="">
                    <p>Nueva Temporada</p>
                </a>
            </div>
       </div>
       <div class="seccion-sliders">
            <?php echo do_shortcode('[woo_product_slider id="104"]');?>
       </div>
       <div class="seccion-sliders">
            <?php echo do_shortcode('[woo_product_slider id="115"]');?>
       </div>
       <div class="seccion-sliders">
            <?php echo do_shortcode('[woo_product_slider id="116"]');?>
       </div>
    </div>
    
</div>

<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>