<?php

/**
 * Template Name: Pagina Contactanos
 */

/*Éste es el archivo de plantilla que mostrará por 
defecto cualquier página que creemos, siempre y cuando 
no se le haya especificado una plantilla.*/
?>
<?php
/*Es la plantilla que WordPress carga por defecto 
como página de inicio. Está especialmente pensada 
para que sea un listado de posts, es decir, la 
portada de un blog.*/
?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
    <div class="contenedorContactanos">
        <?php do_action( 'woocommerce_before_main_content' ); ?>
        <div class="titulo">
            <h3>Contáctanos</h3>
        </div>
        <div class="contacto">
            <div class="cont-contacto">
                <div class="dudas">
                    <p class="titulo">¿Tienes alguna duda?</p>
                    <p class="texto">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem velit omnis facere laborum fugiat, nobis officiis expedita ab asperiores sed nostrum aperiam beatae quia odio. Aspernatur impedit hic dicta minus.
                    Cupiditate, praesentium? Tempora sit mollitia minima distinctio impedit inventore, dolorum nostrum cumque atque ipsum ipsa ratione sint voluptate doloribus temporibus quidem, sunt quia non unde illo? Eum reprehenderit itaque provident.
                    Nostrum cum ullam enim ad, autem earum? Neque molestias enim perspiciatis sit eum! Quidem incidunt cum eum, qui dignissimos voluptatem corrupti numquam? Vitae sed nam tenetur quidem iusto nihil eligendi!</p>
                </div>
                <div class="formas-contacto">
                    <div class="cont-telefono">
                        <span class="icon-telefono"></span>
                        <a href="#" class="text-telefono">614-123-45-67</a>
                    </div>
                    <div class="cont-email">
                        <span class="icon-email"></span>
                        <a href="#" class="text-email">elzapatito@gmail.com</a>
                    </div>
                    <div class="redes">
              
                        <span class="face-icon"></span>
                        <a href="#">@zapatito</a>
            
            
                        <span class="insta-icon"></span>
                        <a href="#">@instagram</a>
                    
                    </div>
                    
                </div>

            </div>
            <div class="form-contacto">
                <div class="cont-form">
                    <h3 class="titulo">Envíanos un mensaje</h3>
                    <?php echo do_shortcode('[contact-form-7 id="171" title="Form contacto"]'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>