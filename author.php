<?php
/*Sirve para mostrar un listado de posts de una categoría específica.*/
?>

<?php

/* Será la plantilla que mostrará WordPress en caso 
de tener activa la opción Mostrar página estática como 
página de inicio. Este archivo está pensado para casos 
en los que la página de inicio es una página estática, 
como puede ser la página de inicio de un site corporativo. 

get_header(), una función que incluye el archivo header.php 
get_footer() que figura al final del código, y que incorpora el archivo footer.php.
get_sidebar() incluirá en la plantilla el archivo sidebar.php.
have_posts() que comprueba si hay posts para mostrar.
the_post() posibilita el acceso todas sus propiedades con funciones nativas de WordPress del post
the_permalink() Devuelve el enlace completo del post.
the_title() Muestra el título del post.
the_time() Permite mostrar la fecha de publicación del post y permite personalizar el formato de la fecha.
the_excerpt() Muestra un número limitado de caracteres del contenido del post. 
the_author_posts_link() Muestra el nombre del autor del post y enlaza a su página de perfil.
 next_post_link() y previous_post_link(), que mostrarán los enlaces para paginar los posts. 
*/

?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Autor -->
<p>Posts de <strong><?php echo get_the_author(); ?></strong></p>
<!-- Listado de posts -->
<?php if (have_posts()) : ?>
    <section>
        <?php while (have_posts()) : the_post(); ?>
            <article>
                <header>
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    <?php the_post_thumbnail(); ?>
                    <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j F, Y'); ?></time>
                    <?php the_category(); ?>
                </header>
                <?php the_excerpt(); ?>
                <footer>
                    <address>Por <?php the_author_posts_link() ?></address>
                </footer>
            </article>
        <?php endwhile; ?>
        <div class="pagination">
            <span class="in-left"><?php next_posts_link('« Entradas antiguas'); ?></span>
            <span class="in-right"><?php previous_posts_link('Entradas más recientes »'); ?></span>
        </div>
    </section>
<?php else : ?>
    <p><?php _e('Ups!, no hay entradas.'); ?></p>
<?php endif; ?>
<!-- Archivo de barra lateral por defecto -->
<?php get_sidebar(); ?>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>