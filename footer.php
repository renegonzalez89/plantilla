<?php wp_footer(); ?>
</div><!-- cierre del div principal -->
<footer>
    <div class="contenedorBoletin">
        <div class="datosBoletin">
            <?php echo do_shortcode('[content_newsletter]');?>
        </div>
    </div>
    <div class="ContenedorDatosEmperesa">
        <div class="datosEmperesa">
            <div class="logo">
                <div class="img">
                    <img src="<?php echo get_theme_file_uri('asset/src/inicio/logoBlanco.png'); ?>" alt="logotipo">
                </div>
                <div class="descripcion">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda deserunt corporis autem id pariatur, eveniet sint commodi accusantium esse vero voluptas itaque soluta molestias! Ad!</p>
                </div>
            </div>
        </div>
        <div class="enlaces">
            <div class="datosEnlaces">
                <ul>
                    <li>
                        <a href="<?php echo site_url('seguridad');?>">
                            Seguridad
                        </a>    
                    </li>
                    <li>
                        <a href="<?php echo site_url('preguntas-frecuentes');?>">
                            Preguntas frecuentes
                        </a>    
                    </li>
                    <li>
                        <a href="<?php echo site_url('devoluciones');?>">
                            Devoluciones
                        </a>    
                    </li>
                    <li>
                        <a href="<?php echo site_url('formas-de-pago');?>">
                            Formas de pago
                        </a>    
                    </li>
                    <li>
                        <a href="<?php echo site_url('terminos-y-condiciones');?>">
                            Terminos y Condiciones
                        </a>    
                    </li>
                    <li>
                        <a href="<?php echo site_url('aviso-de-privacidad');?>">
                            Aviso de Privacidad
                        </a>    
                    </li>
                </ul>
            </div>
        </div>
        <div class="contacto">
            <div class="datosContacto">
                <ul>
                    <li class="telefono">
                        <a href="">614-123-45-67</a>
                    </li>
                    <li class="email">
                        <a href="">elzapatito@email.com</a>
                    </li>
                    <li class="facebook">
                        <a href="">@Facebook</a>
                    </li>
                    <li class="instagram">
                        <a href="">@Instagram</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="contenedorDerechos">
        <div class="datosDerechos">
            <p>Creado por Don Mesero® 2022</p>
        </div>
    </div>
</footer>
</body>
</html>