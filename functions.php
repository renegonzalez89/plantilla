<?php

/* Este archivo nos permitirá crear zonas de menú y de widgets, 
así como personalizar algunos parámetros que WordPress trae por defecto.

register_nav_menus(). Mediante un array, añadiremos los menús que necesitamos.
add_action(). En ella, indicaremos en el primer parámetro cuándo se desea ejecutar la función y con el segundo parámetro el nombre de la función que se debe ejecutar (es decir, mis_menus).
*/
?>

<?php
/**
 * Agregar nuestros propios estilos
 */
function agregar_css_js()
{
    wp_enqueue_style('Montserrat', ' href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;800;900&display=swap" rel="stylesheet"');
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('gliderCSS', get_theme_file_uri('asset/librerias/Glider/glider.css'));
    wp_enqueue_style('estiloshf', get_theme_file_uri('asset/css/style-hf.css'));
    wp_enqueue_style('estilosInicio', get_theme_file_uri('asset/css/style-inicio.css'));
    wp_enqueue_style('estilosSingleProducts', get_theme_file_uri('asset/css/style-single-productos.css'));
    wp_enqueue_style('estilosCategoriaProducts', get_theme_file_uri('asset/css/style-categorias-productos.css'));
    wp_enqueue_style('estilosTexto', get_theme_file_uri('asset/css/style-texto.css'));
    wp_enqueue_style('estilosConocenos', get_theme_file_uri('asset/css/style-conocenos.css'));
    wp_enqueue_style('estilosContacto', get_theme_file_uri('asset/css/style-contacto.css'));
   
    if (is_page('carrito')) {
       wp_enqueue_style('estilosCarrito', get_theme_file_uri('asset/css/style-carrito.css'));
   }
   if (is_page('finalizar-compra')) {
        wp_enqueue_style('estilosPagar', get_theme_file_uri('asset/css/style-pagar.css'));
   }
   if (is_page('mi-cuenta')) {
       wp_enqueue_style('estilosCuenta', get_theme_file_uri('asset/css/style-mi-cuenta.css'));
   }
    wp_enqueue_script('fontawesome', get_theme_file_uri('asset/librerias/fontawesome.min-V6.js'), '6.0.0', true);
    //wp_enqueue_script('fontawesome', get_theme_file_uri('asset/librerias/fontawesome.min-V5.js'), '5.15.4', true);
    wp_enqueue_script('glider', get_theme_file_uri('asset/librerias/Glider/glider.js'), '1.7.4', true);

    wp_enqueue_script('scripts', get_theme_file_uri('asset/js/script.js'), '1.0.0', true);
    wp_enqueue_script('carousel', get_theme_file_uri('asset/js/carousel.js'),array('glider'), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'agregar_css_js');

/**
 * Crear nuestros menús gestionables desde el
 * administrador de Wordpress.
 */

function mis_menus()
{
    register_nav_menus(
        array(
            'navegation' => __('Menú de navegación'),
        )
    );
    register_nav_menus(
        array(
            'catalogo' => __('Menú catalogo'),
        )
    );
    register_nav_menus(
        array(
            'login' => __('Menú login'),
        )
    );
}
add_action('init', 'mis_menus');

/**
 * Crear una zonan de widgets que podremos gestionar
 * fácilmente desde administrador de Wordpress.
 * 
 * add_action(), igual que hicimos con el menú. Dentro de nuestra función mis_wigets, 
 * llamaremos a la función nativa de WordPress register_sidebar(), que nos permite crear 
 * una zona de widgets y que nos permite configurar varios parámetros enviándole un elemento 
 * de array.
 * 
 * name Es el nombre de la zona creada, que nos aparecerá en el gestor de widgets. 
 * id Es el identificador que nos permitirá incluir esta zona de widgets dentro de una plantilla.
 * before_widget Nos permite añadir código HTML antes de cada widget añadido a nuestra zona. Para este caso, hemos añadido un div contenedor con la clase widget. 
 * after_widget Funciona igual que el elemento anterior, pero en este caso no permite añadir código después de cada widget. Lo utilizaremos para cerrar el contenedor abierto previamente. 
 * before_title Los widgets por defecto llevan un título que, por lo general, puede editarse. Mediante este parámetro, podemos añadir código HTML antes de cada título de los widgets, como por ejemplo una etiqueta <h3>.
 * after_title Podemos agregar código después del título, y aprovechamos para cerrar el <h3> abierto anteriormente.
 */

// Registro de la zona de Widgets llamada: Between Content left
register_sidebar( array(
    'id' => 'content_ofertas',
    'name' => __( 'Between Content'),
    'description' => __( 'Area para los widgets del zona Izquirda'),
    'before_widget' => '<div class="contenedor-ofertas">',
    'after_widget' => '</div>',
) );

// Creación del shortcode: [content_ofertas]
add_shortcode( 'content_ofertas', 'dcms_get_content_ofertas_area' );
function dcms_get_content_ofertas_area() {
    ob_start();
    dynamic_sidebar( 'content_ofertas', array(
	    'before' => '<div class="content_ofertas widget-area">',
	    'after'  => '</div>',
    ) );
    $dcms_content_ofertas = ob_get_contents();
    ob_end_clean();

    return $dcms_content_ofertas;
}
// Registro de la zona de Widgets llamada: Between Content right
register_sidebar( array(
    'id' => 'content_sliders',
    'name' => __( 'Between Content right'),
    'description' => __( 'Area para los widgets del zona Derecha'),
    'before_widget' => '<div class="contenedor-sliders">',
    'after_widget' => '</div>',
) );

// Creación del shortcode: [content_sliders]
add_shortcode( 'content_sliders', 'dcms_get_content_sliders_area' );
function dcms_get_content_sliders_area() {
    ob_start();
    dynamic_sidebar( 'content_sliders', array(
	    'before' => '<div class="content_sliders widget-area">',
	    'after'  => '</div>',
    ) );
    $dcms_content_sliders = ob_get_contents();
    ob_end_clean();

    return $dcms_content_sliders;
}
// Registro de la zona de Widgets llamada: widgets mostrados en el categoria
register_sidebar( array(
    'id' => 'content_widgets_categoria',
    'name' => __( 'Contenido categorias'),
    'description' => __( 'Area para los widgets de la zona izquierda de las categorias'),
    'before_widget' => '<div class="contenedor_widgets_categorias">',
    'after_widget' => '</div>',
) );

// Creación del shortcode: [content_widgets_categoria]
add_shortcode( 'content_widgets_categoria', 'dcms_get_content_widgets_categoria_area' );
function dcms_get_content_widgets_categoria_area() {
    ob_start();
    dynamic_sidebar( 'content_widgets_categoria', array(
	    'before' => '<div class="content_widgets_categoria widget-area">',
	    'after'  => '</div>',
    ) );
    $dcms_content_widgets_categoria = ob_get_contents();
    ob_end_clean();

    return $dcms_content_widgets_categoria;
}
// Registro de la zona de Widgets llamada: footer
register_sidebar( array(
    'id' => 'content_newsletter',
    'name' => __( 'Contenido footer'),
    'description' => __( 'Area para los widgets del zona del footer'),
    'before_widget' => '<div class="contenedor-newsletter">',
    'after_widget' => '</div>',
) );

// Creación del shortcode: [content_newsletter]
add_shortcode( 'content_newsletter', 'dcms_get_content_newsletter_area' );
function dcms_get_content_newsletter_area() {
    ob_start();
    dynamic_sidebar( 'content_newsletter', array(
	    'before' => '<div class="content_newsletter widget-area">',
	    'after'  => '</div>',
    ) );
    $dcms_content_newsletter = ob_get_contents();
    ob_end_clean();

    return $dcms_content_newsletter;
}
/**
 * Filtrar resultados de búsqueda para que solo muestre
 * posts en el listado


*function buscar_solo_posts($query)
*{
*    if ($query->is_search) {
*        $query->set('post_type', 'post');
*    }
*    return $query;
*}
*add_filter('pre_get_posts', 'buscar_solo_posts');
 */
/**
 * crear compatibilidad con woocommerce
 */

add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce');
}

/** 
 * Para deshabilitar los estilos predeterminados de WooCommerce
 *if (class_exists('Woocommerce')){
  *   add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
 *}
*/

/**
 * agregando cantidad al carrito
 */
add_filter('woocommerce_add_to_cart_fragments', 'actualizar_minicarro_ajax', 10, 1);
function actualizar_minicarro_ajax($fragments)
{
    $fragments['div.cont-minicarro'] = '<div class="cont-minicarro"><span id="items-minicarro">' . WC()->cart->get_cart_contents_count() . '</span></div>';
    return $fragments;
}

/**
 * cambiando elementos de productos woocommerce
 */

 /** cambiar de posicion el precio */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );


/* Cambiar el número máximo de productos relacionados en la ficha de producto 
function ejr_woo_limite_relacionados () {
  global $product;
 
    $args = array (
        'post_type'             => 'producto',
        'no_found_rows'         => 1,
        'posts_per_page'        => 2,
        'ignore_sticky_posts'   => 1,
        'orderby'               => $orderby,
        'post__in'              => $related,
        'post__not_in'          => array($product->id)
    );
    return $args;
}
add_filter ('woocommerce_related_products_args', 'ejr_woo_limite_relacionados');
*/

// modificar cuantos productos se muestran en la pagian de categorias //

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options –> Reading
  // Return the number of products you wanna show per page.
  $cols = 15;
  return $cols;
}

//* Integra migas de pan a WordPress sin plugin
function migas_de_pan() {
    if (!is_front_page()) {
       echo '<a href="/">Inicio</a> > ';
       if (is_category() || is_single() || is_page()) {
          if(is_category()){
             $category = get_the_category();
          echo $category[0]->cat_name;
          }else{
              the_category(' - ');
          }if(is_page()) {
              echo the_title();
          }if (is_single()) {
              echo " > ";
              the_title();
          }
       }
    }
}