<?php

/**
 * Template Name: Pagina conocenos
 */

/*Éste es el archivo de plantilla que mostrará por 
defecto cualquier página que creemos, siempre y cuando 
no se le haya especificado una plantilla.*/
?>
<?php
/*Es la plantilla que WordPress carga por defecto 
como página de inicio. Está especialmente pensada 
para que sea un listado de posts, es decir, la 
portada de un blog.*/
?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
    <div class="contenedorConocenos">
        <?php do_action( 'woocommerce_before_main_content' ); ?>
        <h3 class="titulo">Quienes somos</h3>
        <div class="informacion">
            <div class="imagen">
                <img src="<?php echo get_theme_file_uri('asset/src/inicio/trabajador.png'); ?>" alt="">
            </div>
            <div class="contenedor-informacion">
                <div class="giroEmpresa">
                    <div class="mision">
                        <h3>Misión</h3>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit numquam enim accusantium at architecto. Excepturi sunt, temporibus deserunt ullam dignissimos blanditiis, odit voluptatem inventore libero repellendus alias tempora tenetur rerum?
                            Libero illo, labore facilis enim alias animi totam optio esse voluptates aliquam a. Incidunt architecto non ipsa eum reiciendis autem eaque quidem fugiat hic eligendi, enim, qui tenetur. Natus, aliquam.
                            Dolor atque laudantium perspiciatis temporibus ipsa ducimus! Porro, et eveniet? Aperiam laudantium eius voluptatem quibusdam ut. Sequi labore, tenetur, hic at enim et qui facilis sapiente saepe nisi impedit pariatur.
                            Sapiente, labore debitis aliquam aperiam eius quae maiores illo, tempore pariatur alias autem excepturi at tempora animi voluptatem ullam quos, similique iusto veniam adipisci totam! Commodi quo corrupti a itaque?
                        </p>
                    </div>
                    <div class="vision">
                        <h3>Visión</h3>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi minus voluptates omnis ad perspiciatis eligendi, distinctio obcaecati alias quidem numquam, ipsum aut recusandae vitae architecto mollitia ipsa fuga veniam libero!
                            Eveniet excepturi quis, maiores ex aut architecto unde quod culpa distinctio id repudiandae quasi pariatur consequuntur eius omnis est corrupti enim tenetur expedita laborum optio at autem esse quo. Minima?
                            Inventore, vitae molestiae. Aspernatur voluptatum eius recusandae est at consequuntur velit ea ipsam earum temporibus voluptates hic rem iste asperiores exercitationem, dicta error cupiditate dignissimos quos tempora adipisci, nostrum labore.
                            Omnis et, sequi alias facere, consequatur suscipit, at iusto totam dicta natus explicabo iste nobis quae vero amet fuga corporis? Vitae a autem ratione tempore quae fugit illo, laborum eos.
                        </p>
                    </div>
                </div>
                <div class="logos">
                    <div class="logo-uno tamaño">
                        <img src="<?php echo get_theme_file_uri('asset/src/inicio/logotipo.png'); ?>" alt="">
                    </div>
                    <div class="logo-dos tamaño">
                        <img src="<?php echo get_theme_file_uri('asset/src/inicio/logo-03.png'); ?>" alt="">
                    </div>
                    <div class="logo-tres tamaño">
                        <img src="<?php echo get_theme_file_uri('asset/src/inicio/logo-05.png'); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="redes-sociales">
            <span class="facebook-icon"></span>
            <a class="facebook-link" href="#">@zapatito</a>
            <span class="instagram-icon"></span>
            <a class="instagram-link" href="#">@zapatito</a>
        </div>
    </div>
<?php get_footer(); ?>