<?php
/* Es la plantilla que muestra un post completo por defecto.

 comments_template(). Con ella, se llama al archivo comment-template.php del directorio wp-includes de WordPress.
*/


?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Contenido del post -->
<?php if (have_posts()) : the_post(); ?>
    <section>
        <!-- titulo del post -->
        <h1><?php the_title(); ?></h1>
         <!-- imagen del post -->
        <?php the_post_thumbnail(); ?>
        <!-- fecha creacion del post -->
        <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j F, Y'); ?></time>
        <!-- categorias -->
        <?php the_category (); ?>
        <!-- conteido -->
        <?php the_content(); ?>
        <address>Por <?php the_author_posts_link() ?></address>
        <!-- Etiquetas -->
        <?php the_tags(); ?> 
        <!-- Comentarios -->
        <?php comments_template(); ?>
    </section>
<?php else : ?>
    <p><?php _e('Ups!, esta entrada no existe.'); ?></p>
<?php endif; ?>
<!-- Archivo de barra lateral por defecto -->
<?php get_sidebar(); ?>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>