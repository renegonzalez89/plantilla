<?php

/**
 * Template Name: Plantilla de Contacto
 */

/*Éste es el archivo de plantilla que mostrará por 
defecto cualquier página que creemos, siempre y cuando 
no se le haya especificado una plantilla.*/
?>
<?php
/*Es la plantilla que WordPress carga por defecto 
como página de inicio. Está especialmente pensada 
para que sea un listado de posts, es decir, la 
portada de un blog.*/
?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Contenido de página de inicio -->
<?php if (have_posts()) : the_post(); ?>
    <section>
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <form method="post" action="">
            <label for="name">Nombre</label><br>
            <input type="text" id="name" placeholder="Escribe aquí tu nombre y apellidos"><br>
            <label for="subject">Asunto</label><br>
            <input type="text" id="subject" placeholder="Motivo de tu consulta"><br>
            <label for="message">Mensaje</label><br>
            <textarea id="message"></textarea><br>
            <button type="submit">Enviar datos</button><br>
        </form>
    </section>
<?php endif; ?>
<!-- Archivo de barra lateral por defecto -->
<?php get_sidebar(); ?>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>