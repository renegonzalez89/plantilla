<?php

/**
 * Template Name: Pagina catalogos
 */

/*Éste es el archivo de plantilla que mostrará por 
defecto cualquier página que creemos, siempre y cuando 
no se le haya especificado una plantilla.*/
?>
<?php
/*Es la plantilla que WordPress carga por defecto 
como página de inicio. Está especialmente pensada 
para que sea un listado de posts, es decir, la 
portada de un blog.*/
?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
    <h3>Pagina de catalogos</h3>
<?php get_footer(); ?>