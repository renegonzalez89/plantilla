<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
    <?php wp_head(); ?>
</head>

<body>
    <div class="contenedor-principal">
        <header>
            <nav class="navLogin">
                <ul class="main-nav">
                    <?php wp_nav_menu(array('theme_location' => 'login')); ?>
                </ul>
            </nav>
            <div class="navLogo">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><img src="<?php echo get_theme_file_uri('asset/src/inicio/logotipo.png'); ?>" alt="logo"></a>
                </div>
                <div class="buscador">
                    <?php echo do_shortcode('[fibosearch]'); ?>
                </div>
                <div class="navbar-carro">
                    <a href="<?php echo site_url('carrito'); ?>" title="carrito">
                    <div class="cont-minicarro">
                        <span id="items-minicarro"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                    </div>
                    </a>
                </div>
            </div>
            <nav class="navPrincipal">
                <ul class="main-nav">
                    <?php wp_nav_menu(array('theme_location' => 'navegation')); ?>
                </ul>
            </nav>
            
            
        </header>